DESCRIPTION
-----------

IconBox module can be used to create different Content box with Icons.
You can use Iconbox block or Shortcodes to see Iconbox in action.

You can try out live demonstration of it here:
http://corporate.drupalchamp.com/


INSTALLATION
------------

1. Download & extract "Font-Awesome" from http://fortawesome.github.com/Font-Awesome/
and place inside "sites/all/libraries/fontawesome" directory. CSS file should be
sites/all/libraries/fontawesome/css/font-awesome.css

2. Enable the module at admin/modules/
   (http://drupal.org/documentation/install/modules-themes/modules-7)

3. Configuration: admin/config/user-interface/iconbox/

4. For Shortcode: admin/config/content/formats/ , configure [html_format] and enable Iconbox at 'Enabled filters' section.


That's it!


MAINTAINERS
------------
developmenticon.com, drupalchamp.com
